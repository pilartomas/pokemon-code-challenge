import { FC } from "react";
import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import { grey } from "@mui/material/colors";
import { VolumeUp } from "@mui/icons-material";
import cx from "classnames";
import { PokemonCardAttribute } from "./PokemonCardAttribute";
import { PokemonCardLinearAttribute } from "./PokemonCardLinearAttribute";
import { Pokemon } from "../api/generated/graphql";
import { PokemonFavoriteButton } from "./PokemonFavoriteButton";
import styles from "./PokemonCard.module.scss";
import { usePrevious } from "../hooks/usePrevious";

export type PokemonCardProps = {
  className?: string;
  animate?: boolean;
  pokemon: Pick<Pokemon, "id" | "name" | "image" | "isFavorite"> &
    Partial<
      Pick<Pokemon, "types" | "maxCP" | "maxHP" | "weight" | "height" | "sound">
    >;
};

export const PokemonCard: FC<PokemonCardProps> = ({
  className,
  animate,
  pokemon: {
    id,
    image,
    name,
    types,
    isFavorite,
    maxCP,
    maxHP,
    weight,
    height,
    sound,
  },
}) => {
  const wasFavorite = usePrevious(isFavorite);
  const becameFavorite = wasFavorite === false && isFavorite;
  const stoppedBeingFavorite = wasFavorite === true && !isFavorite;

  const playSound = () => {
    if (sound) {
      const audio = new Audio(sound);
      audio.play();
    }
  };

  return (
    <Card
      sx={{
        maxWidth: 600,
        minWidth: 200,
        width: "100%",
      }}
      className={className}
    >
      <CardMedia sx={{ position: "relative" }}>
        <Link href={name}>
          <Image
            src={image}
            alt={name}
            height={200}
            width={200}
            style={{
              display: "block",
              margin: "auto",
            }}
            className={cx(
              animate && becameFavorite && styles.AnimateHappy,
              animate && stoppedBeingFavorite && styles.AnimateAngry
            )}
          />
        </Link>
        {sound && (
          <IconButton
            onClick={playSound}
            color="primary"
            sx={{ position: "absolute", left: 0, bottom: 0 }}
          >
            <VolumeUp />
          </IconButton>
        )}
      </CardMedia>
      <CardContent sx={{ backgroundColor: grey[50] }}>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Stack>
            <Typography variant="h5">{name}</Typography>
            {types && (
              <Typography variant="body2">{types.join(", ")}</Typography>
            )}
          </Stack>
          <PokemonFavoriteButton pokemon={{ id, name, isFavorite }} />
        </Stack>
        {maxCP && (
          <PokemonCardLinearAttribute
            name="CP"
            value={maxCP}
            color="secondary"
          />
        )}
        {maxHP && (
          <PokemonCardLinearAttribute name="HP" value={maxHP} color="primary" />
        )}
        <Grid container spacing={1}>
          {weight && (
            <Grid item xs={12} sm={6}>
              <PokemonCardAttribute
                name="Weight"
                value={`${weight.minimum} - ${weight.maximum}`}
              />
            </Grid>
          )}
          {height && (
            <Grid item xs={12} sm={6}>
              <PokemonCardAttribute
                name="Height"
                value={`${height.minimum} - ${height.maximum}`}
              />
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Card>
  );
};
