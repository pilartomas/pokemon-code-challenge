import { Visibility } from "@mui/icons-material";
import {
  Avatar,
  Box,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Modal,
} from "@mui/material";
import { FC, useState } from "react";
import { Pokemon } from "../api/generated/graphql";
import { PokemonCard, PokemonCardProps } from "./PokemonCard";
import { PokemonFavoriteButton } from "./PokemonFavoriteButton";

export type PokemonListItemProps = {
  pokemon: Pick<Pokemon, "id" | "name" | "image" | "isFavorite"> &
    PokemonCardProps["pokemon"];
};

export const PokemonListItem: FC<PokemonListItemProps> = ({ pokemon }) => {
  const [modalOpen, setModalOpen] = useState(false);

  const { name, image } = pokemon;

  return (
    <>
      <ListItem button component="a" href={name}>
        <ListItemAvatar>
          <Avatar alt={name} src={image} />
        </ListItemAvatar>
        <ListItemText primary={name} />
        <ListItemSecondaryAction>
          <IconButton onClick={() => setModalOpen(true)}>
            <Visibility />
          </IconButton>
          <PokemonFavoriteButton pokemon={pokemon} />
        </ListItemSecondaryAction>
      </ListItem>
      <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
        <Box
          sx={{
            position: "fixed",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 300, 
          }}
        >
          <PokemonCard pokemon={pokemon} />
        </Box>
      </Modal>
    </>
  );
};
