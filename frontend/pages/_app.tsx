import { ApolloProvider } from "@apollo/client";
import type { AppProps } from "next/app";
import { Container, CssBaseline, ThemeProvider } from "@mui/material";
import { SnackbarProvider } from "notistack";
import { client } from "../api/client";
import { theme } from "../styles/theme";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <SnackbarProvider maxSnack={3}>
          <Container maxWidth="md">
            <Component {...pageProps} />
          </Container>
        </SnackbarProvider>
      </ThemeProvider>
    </ApolloProvider>
  );
}
