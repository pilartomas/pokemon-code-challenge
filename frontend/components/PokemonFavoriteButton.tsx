import { useMutation } from "@apollo/client";
import { Favorite, FavoriteBorder } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { useSnackbar } from "notistack";
import { FC } from "react";
import {
  Pokemon,
  TogglePokemonFavoriteDocument,
} from "../api/generated/graphql";

export type PokemonFavoriteButtonProps = {
  pokemon: Pick<Pokemon, "id" | "name" | "isFavorite">;
};

export const PokemonFavoriteButton: FC<PokemonFavoriteButtonProps> = ({
  pokemon: { id, name, isFavorite },
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [toggleFavorite] = useMutation(TogglePokemonFavoriteDocument, {
    // Evict pokemons field cache if isFavorite filter has been used
    update: (cache, _) => {
      // TODO very fragile implementation, find a different way
      const id = "ROOT_QUERY";
      const fieldName = "pokemons";
      const rootQuery = (cache as any).data.data[id];
      Object.keys(rootQuery).forEach((key) => {
        if (key.startsWith(fieldName + ":") && key.includes("isFavorite")) {
          const args = JSON.parse(key.slice(fieldName.length + 1));
          cache.evict({
            id,
            fieldName,
            args,
          });
        }
      });
    },
    onCompleted: () => {
      enqueueSnackbar(`${name}'s favorite status changed`);
    },
    onError: () => {
      enqueueSnackbar(`Failed to change ${name}'s favorite status`, {
        variant: "error",
      });
    },
  });

  return (
    <IconButton
      onClick={() =>
        toggleFavorite({ variables: { id, setFavorite: !isFavorite } })
      }
      color="error"
    >
      {isFavorite ? <Favorite /> : <FavoriteBorder />}
    </IconButton>
  );
};
