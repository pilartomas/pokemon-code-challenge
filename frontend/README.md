## Getting started

Before running the app for the first time, start the backend server and run

```bash
npm run codegen
```

To run the development server:

```bash
npm run dev
```

## Writeup

- Chosen stack is NextJS + Apollo Client + Material UI + GraphQL codegen
- All the desired features including pagination are implemented, except for navigation when clicking pokemon name.
- I admit there is not much CSS code (not to speak about SASS). It's mostly scaterred throughout the components but I've added some animations and transform.
- `PokemonCard` and `PokemonListItem` props could be refactored into receiving just the pokemon ID. This should go hand in hand with more cache optimization (e.g. we can utilize the fact that pokemon name is/seems unique).
- Most intriguing problem to solve was combination of pagination an favorites mutation. We want the cache to use `merge` refetching strategy instead of default `overwrite` in order to keep paginated data. However, this makes refetching after `TogglePokemonFavorite` mutation incorrect. The items of the list have changed if appropriate filter is used. I've "solved" it with targeted cache eviction from the mutation but as you can see, the code depends on cache key structure and might use hidden cache API (prone to change).

## TODOs

- SSG for the landing page (and frequently accessed pokemons?).
- Disable queries during SSR, keep it client-only.
