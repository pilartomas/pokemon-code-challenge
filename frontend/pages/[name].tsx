import { useQuery } from "@apollo/client";
import { Box, CircularProgress, Grid, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { GetPokemonByNameDocument } from "../api/generated/graphql";
import { PokemonCard } from "../components/PokemonCard";

type PokemonDetailsParams = {
  name?: string;
};

export default function PokemonDetails() {
  const router = useRouter();
  const { name } = router.query as PokemonDetailsParams;

  const { data, loading, error } = useQuery(GetPokemonByNameDocument, {
    variables: { name: name! },
    skip: !name,
  });

  if (error) return error;
  if (loading || !data) return <CircularProgress />;
  if (!data.pokemonByName) return `No pokemon named ${name} found`;

  const pokemon = data.pokemonByName;

  return (
    <Stack spacing={2} alignItems="center">
      <PokemonCard pokemon={pokemon} animate />
      {pokemon.evolutions.length > 0 && (
        <Box>
          <Typography variant="h6" gutterBottom>
            Evolutions
          </Typography>
          <Grid container spacing={2}>
            {pokemon.evolutions.map((pokemon) => (
              <Grid key={pokemon.id} item xs={12} sm={6}>
                <PokemonCard pokemon={pokemon} />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}
    </Stack>
  );
}
