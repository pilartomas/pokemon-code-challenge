import { FC } from "react";
import { Paper, Stack, Typography } from "@mui/material";

export type PokemonCardAttributeProps = {
  name: string;
  value: string | number;
};

export const PokemonCardAttribute: FC<PokemonCardAttributeProps> = ({
  name,
  value,
}) => {
  return (
    <Paper sx={{ backgroundColor: "inherit" }}>
      <Stack alignItems="center">
        <Typography variant="h5">{name}</Typography>
        <Typography variant="body1">{value}</Typography>
      </Stack>
    </Paper>
  );
};
