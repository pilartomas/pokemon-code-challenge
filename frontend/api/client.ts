import { ApolloClient, InMemoryCache } from "@apollo/client";
import { DeepPartial } from "../utils/types";
import { PokemonConnection, QueryPokemonsArgs } from "./generated/graphql";

export const client = new ApolloClient({
  uri: process.env.NEXT_PUBLIC_API_URL,
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          pokemons: {
            keyArgs: ["query", ["search", "filter", ["type", "isFavorite"]]],
            merge(
              existing: DeepPartial<PokemonConnection> | undefined,
              incoming: DeepPartial<PokemonConnection> | undefined,
              { args }
            ): DeepPartial<PokemonConnection> {
              const typedArgs = args as QueryPokemonsArgs | null;
              const edges = existing?.edges ? existing.edges.slice(0) : [];
              if (incoming?.edges) {
                if (typedArgs) {
                  const offset = typedArgs.query.offset ?? 0;
                  for (let i = 0; i < incoming.edges.length; ++i) {
                    edges[offset + i] = incoming.edges[i];
                  }
                }
              }
              return {
                ...existing,
                ...incoming,
                edges: edges,
              };
            },
          },
        },
      },
    },
  }),
});
