import {
  Box,
  LinearProgress,
  LinearProgressProps,
  Typography,
} from "@mui/material";
import { FC } from "react";

export type PokemonCardLinearAttributeProps = {
  name: string;
  value: string | number;
  color: LinearProgressProps["color"];
};

export const PokemonCardLinearAttribute: FC<
  PokemonCardLinearAttributeProps
> = ({ name, value, color }) => {
  return (
    <Box sx={{ display: "flex", alignItems: "center", width: "100%" }}>
      <Box sx={{ width: "100%", mr: 1 }}>
        <LinearProgress variant="determinate" color={color} value={100} />
      </Box>
      <Box sx={{ minWidth: 80 }}>
        <Typography variant="body2">
          {name}: {value}
        </Typography>
      </Box>
    </Box>
  );
};
