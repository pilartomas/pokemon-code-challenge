import React, { useState } from "react";
import { useQuery } from "@apollo/client";
import {
  CircularProgress,
  FormControl,
  Grid,
  InputLabel,
  List,
  MenuItem,
  Paper,
  Select,
  Stack,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from "@mui/material";
import { ViewList, ViewModule } from "@mui/icons-material";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import {
  ListPokemonsDocument,
  ListPokemonTypesDocument,
} from "../api/generated/graphql";
import { PokemonCard } from "../components/PokemonCard";
import { PokemonListItem } from "../components/PokemonListItem";
import styles from "./index.module.scss";
import { useReverseDebounce } from "../hooks/useReverseDebounce";

enum ViewType {
  List,
  Grid,
}

export default function PokemonSearch() {
  const [viewType, setViewType] = useState<ViewType>(ViewType.Grid); // TODO consider using local apollo state

  const pokemonTypesResult = useQuery(ListPokemonTypesDocument);
  const pokemonsResult = useQuery(ListPokemonsDocument, {
    variables: {
      limit: 20,
    },
    refetchWritePolicy: "merge",
    notifyOnNetworkStatusChange: true, // Necessary to get re-render after refetching with new variables
  });

  const [search, setSearch] = useReverseDebounce("", 500, (value) =>
    pokemonsResult.refetch({
      search: value || undefined,
    })
  );

  useBottomScrollListener(() => {
    pokemonsResult.fetchMore({
      variables: {
        offset: pokemonsResult.data?.pokemons.edges.length ?? 0,
      },
    });
  });

  const toolbar = (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <ToggleButtonGroup
          color="primary"
          fullWidth
          exclusive
          value={pokemonsResult.variables?.isFavorite ?? false}
          onChange={(_, value: boolean) =>
            pokemonsResult.refetch({
              isFavorite: value || undefined,
            })
          }
        >
          <ToggleButton value={false}>All</ToggleButton>
          <ToggleButton value={true}>Favorites</ToggleButton>
        </ToggleButtonGroup>
      </Grid>
      <Grid item xs={12} md={7}>
        <TextField
          label="Search"
          type="search"
          variant="outlined"
          fullWidth
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </Grid>
      <Grid item xs={7} md={3}>
        <FormControl fullWidth>
          <InputLabel id="type-select-label">Type</InputLabel>
          <Select
            labelId="type-select-label"
            label="Type"
            value={pokemonsResult.variables?.type ?? ""}
            onChange={(e) =>
              pokemonsResult.refetch({ type: e.target.value || undefined })
            }
          >
            <MenuItem value="">None</MenuItem>
            {pokemonTypesResult.data?.pokemonTypes.map((type) => (
              <MenuItem key={type} value={type}>
                {type}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={5} md={2}>
        <ToggleButtonGroup
          color="primary"
          exclusive
          value={viewType}
          onChange={(_, value: ViewType) => setViewType(value)}
        >
          <ToggleButton value={ViewType.List}>
            <ViewList />
          </ToggleButton>
          <ToggleButton value={ViewType.Grid}>
            <ViewModule />
          </ToggleButton>
        </ToggleButtonGroup>
      </Grid>
    </Grid>
  );

  const dataDisplay = (
    <>
      {pokemonsResult.loading && (
        <CircularProgress sx={{ display: "block", margin: "auto" }} />
      )}
      {pokemonsResult.error && (
        <Typography sx={{ textAlign: "center" }}>
          Unable to catch any pokemons
        </Typography>
      )}
      {pokemonsResult.data && (
        <>
          {viewType === ViewType.Grid && (
            <Grid container spacing={2}>
              {pokemonsResult.data.pokemons.edges.map((pokemon) => (
                <Grid key={pokemon.id} item xs={12} sm={4} md={3}>
                  <PokemonCard pokemon={pokemon} className={styles.Card} />
                </Grid>
              ))}
            </Grid>
          )}
          {viewType === ViewType.List && (
            <List>
              {pokemonsResult.data.pokemons.edges.map((pokemon) => (
                <PokemonListItem key={pokemon.id} pokemon={pokemon} />
              ))}
            </List>
          )}
        </>
      )}
    </>
  );

  return (
    <Stack spacing={2}>
      <Paper>{toolbar}</Paper>
      <Paper>{dataDisplay}</Paper>
    </Stack>
  );
}
