import { createTheme } from "@mui/material";
import { green, purple } from "@mui/material/colors";

export const theme = createTheme({
  palette: {
    primary: {
      main: green[700],
    },
    secondary: {
      main: purple["A200"],
    },
  },
});
