import { useCallback, useEffect, useRef, useState } from "react";

export const useReverseDebounce = <T>(
  initialValue: T,
  delay: number,
  callback: (value: T) => any
) => {
  const [debouncedValue, setDebouncedValue] = useState<T>(initialValue);
  const callbackRef = useRef<typeof callback>(callback);
  const timeoutRef = useRef<NodeJS.Timeout>();

  const setValue = useCallback(
    (value: T) => {
      setDebouncedValue(value);
      if (timeoutRef.current) clearTimeout(timeoutRef.current);
      timeoutRef.current = setTimeout(() => {
        if (callbackRef.current) callbackRef.current(value);
      }, delay);
    },
    [delay]
  );

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  return [debouncedValue, setValue] as const;
};
